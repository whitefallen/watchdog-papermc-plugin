package de.whitefallen.WatchdogPaperMC.Helper;

import com.google.gson.Gson;
import de.whitefallen.WatchdogPaperMC.Class.WatchdogInventory;
import de.whitefallen.WatchdogPaperMC.Http.HTTPPOSTRequest;
import de.whitefallen.WatchdogPaperMC.WatchdogPaperMC;
import org.bukkit.block.Chest;
import org.bukkit.block.DoubleChest;
import org.bukkit.inventory.DoubleChestInventory;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

public class InventoryHelper {

    private final HTTPPOSTRequest postRequest;

    public InventoryHelper(WatchdogPaperMC watchdog) {
        this.postRequest = new HTTPPOSTRequest(watchdog);
    }

    public void sendInventory(Chest chest) {
        WatchdogInventory inv = new WatchdogInventory();
        inv.inventory = Arrays.stream(chest.getInventory().getStorageContents()).filter(Objects::nonNull).map(ItemStack::serialize).collect(Collectors.toList());
        if(chest.getInventory() instanceof DoubleChestInventory) {
            DoubleChest doublechest = (DoubleChest) chest.getInventory().getHolder();
            inv.location = doublechest.getLocation().serialize();
        } else {
            inv.location = chest.getLocation().serialize();
        }
        Gson gson = new Gson();
        String invJson = gson.toJson(inv);
        this.postRequest.sendRequest(invJson);
    }
}
