package de.whitefallen.WatchdogPaperMC;

import de.whitefallen.WatchdogPaperMC.Helper.InventoryHelper;
import de.whitefallen.WatchdogPaperMC.Variables.Watchdog;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.block.DoubleChest;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.DoubleChestInventory;
import org.bukkit.inventory.Inventory;

import static org.bukkit.event.block.Action.RIGHT_CLICK_BLOCK;

public class WatchdogListener implements Listener {
    private final WatchdogPaperMC plugin;
    private final InventoryHelper invHelp;

    public WatchdogListener(WatchdogPaperMC plugin, InventoryHelper invHelp) {
        this.plugin = plugin;
        this.invHelp = invHelp;
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Bukkit.broadcastMessage("Welcome to Server!");
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event)  {
        Player player = event.getPlayer();
        if(Boolean.TRUE.equals(Watchdog.selectMode.get(player.getDisplayName()))) {
            if(event.getAction() == RIGHT_CLICK_BLOCK) {
                Block clickedBlock = event.getClickedBlock();
                if(clickedBlock != null) {
                    Location loc = getChestLocation(clickedBlock);
                    player.sendMessage("Selected Block");
                    Watchdog.selectMode.put(player.getDisplayName(), false);
                    player.sendMessage("interacted");
                    player.sendMessage("You are no longer in Select Mode");
                    if(!Watchdog.watchedBlocks.contains(loc)) {
                        player.sendMessage("Block added");
                        Watchdog.watchedBlocks.add(loc);
                        plugin.getConfig().set("Blocks.Location", Watchdog.watchedBlocks);
                        plugin.saveConfig();
                    } else {
                        player.sendMessage("Block was not added");
                    }
                }
            }
            else {
                event.setCancelled(true);
            }
        }
    }
    public Location getChestLocation(Block block) {
        Location loc = null;
        if(block.getType() == Material.CHEST) {
            Chest chest = (Chest)block.getState();
            Inventory inv = chest.getInventory();
            if(inv instanceof DoubleChestInventory) {
                DoubleChest doublechest = (DoubleChest) chest.getInventory().getHolder();
                loc = doublechest.getLocation();
            } else {
                loc = chest.getLocation();
            }
        }
        return loc;
    }

    @EventHandler
    public void onChestCloses(InventoryCloseEvent event)
    {
        if(event.getInventory().getType().equals(InventoryType.CHEST))
        {
            Block block = event.getInventory().getLocation().getBlock();
            invHelp.sendInventory((Chest)block.getState());
        }
    }
}