package de.whitefallen.WatchdogPaperMC;

import de.whitefallen.WatchdogPaperMC.Commands.CommandWatch;
import de.whitefallen.WatchdogPaperMC.Helper.InventoryHelper;
import de.whitefallen.WatchdogPaperMC.Variables.Watchdog;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

public class WatchdogPaperMC  extends JavaPlugin {

    public WatchdogPaperMC() {
        super();
    }

    @Override
    public void onEnable() {
        super.onEnable();
        loadConfiguration();
        InventoryHelper invHelp = new InventoryHelper(this);
        getServer().getPluginManager().registerEvents(new WatchdogListener(this, invHelp), this);
        this.getCommand("watch").setExecutor(new CommandWatch());
        Object configLoc = this.getConfig().get("Blocks.Location");
        if(configLoc instanceof List && ((List) configLoc).get(0) instanceof Location) {
            Watchdog.watchedBlocks.addAll((List<Location>)configLoc);
        } else if(configLoc instanceof Location) {
            Watchdog.watchedBlocks.add((Location)configLoc);
        }
        new BukkitRunnable() {
            @Override
            public void run() {
                if(!Watchdog.watchedBlocks.isEmpty()) {
                    for (Location loc: Watchdog.watchedBlocks) {
                        Block newBlock = loc.getBlock();
                        if(newBlock.getType() == Material.CHEST) {
                            Chest chest = (Chest)newBlock.getState();
                            invHelp.sendInventory(chest);
                        }
                    }
                }
            }
        }.runTaskTimer(this, 20L, 1200L );

    }

    @Override
    public void onDisable() {

        super.onDisable();
    }

    public void loadConfiguration() {
        this.getConfig().options().copyDefaults(true);
        this.saveConfig();
    }
}
