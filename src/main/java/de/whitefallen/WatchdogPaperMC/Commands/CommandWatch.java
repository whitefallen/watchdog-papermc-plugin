package de.whitefallen.WatchdogPaperMC.Commands;

import de.whitefallen.WatchdogPaperMC.Variables.Watchdog;
import org.bukkit.Bukkit;
import org.bukkit.Warning;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.logging.LogRecord;

public class CommandWatch implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(commandSender instanceof Player) {
            Player player = (Player) commandSender;
            Watchdog.selectMode.put(player.getDisplayName(), true);
            commandSender.sendMessage("You are now in Select Mode");
        }
        return true;
    }
}
