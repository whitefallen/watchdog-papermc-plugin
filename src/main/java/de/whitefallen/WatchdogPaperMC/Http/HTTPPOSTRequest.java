package de.whitefallen.WatchdogPaperMC.Http;

import de.whitefallen.WatchdogPaperMC.WatchdogPaperMC;
import okhttp3.*;
import org.bukkit.Bukkit;

import java.io.IOException;

public class HTTPPOSTRequest {
    private final WatchdogPaperMC plugin;
    public HTTPPOSTRequest(WatchdogPaperMC plugin) {
        this.plugin = plugin;
    }
    public static final MediaType JSON = MediaType.get("application/json; charset=utf-8");
    public static final OkHttpClient client = new OkHttpClient();
    public void sendRequest(String dataJson) {
        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
            RequestBody body = RequestBody.create(JSON, dataJson);
            Request request = new Request.Builder()
                    .url("http://localhost:8000/api/watchBlocks")
                    .post(body)
                    .build();
            try {
                client.newCall(request).execute().close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
